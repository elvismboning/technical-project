from .routes.health import health_router
from .routes.employee import employee_router
from .routes.team import team_router
from .routes.vacation import vacation_router

def add_app_routes(app):
    app.include_router(health_router, prefix="/ping", tags=["Health"])
    app.include_router(employee_router, prefix="/employee", tags=["Employee"])
    app.include_router(team_router, prefix="/team", tags=["Team"])
    app.include_router(vacation_router, prefix="/vacation", tags=["Vacation"])
