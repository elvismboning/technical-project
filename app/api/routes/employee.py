from typing import Optional, List
from uuid import UUID
from fastapi import (
    Depends,
    APIRouter,
    HTTPException
)
from sqlalchemy.orm import Session

from app.db.session import get_db
from app.repository.employee import EmployeeRepository
from app.schema import EmployeeBase

employee_router = APIRouter()

@employee_router.get("/", response_model=List[Optional[EmployeeBase]], status_code=200)
def get_employees(session: Session = Depends(get_db)):
    employees = EmployeeRepository.get_all(session=session)
    if not employees:
        raise HTTPException(status_code=404, detail="Employees not found")
    return employees

@employee_router.get("/{employee_id}", response_model=Optional[EmployeeBase], status_code=200)
def get_employee(session: Session = Depends(get_db), *, employee_id: UUID):
    employee = EmployeeRepository.get_by_id(session=session, employee_id=employee_id)
    if not employee:
        raise HTTPException(status_code=404, detail="Employee not found")
    return employee

@employee_router.post("/", response_model=Optional[EmployeeBase])
def create_employee(session: Session = Depends(get_db), *, employee_data: EmployeeBase):
    employee = EmployeeRepository.get_by_email(session=session, employee_email=employee_data.email)
    if employee:
        raise HTTPException(status_code=400, detail="Email already registered")
    return EmployeeRepository.create(session=session, params=employee_data)
