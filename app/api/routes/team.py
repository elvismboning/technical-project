from typing import Optional
from uuid import UUID
from fastapi import (
    Depends,
    APIRouter,
    status
)
from sqlalchemy.orm import Session

from app.db.session import get_db
from app.repository.team import TeamRepository
from app.schema import TeamBase

team_router = APIRouter()


@team_router.get("/{team_id}", response_model=Optional[TeamBase], status_code=200)
def get_team(session: Session = Depends(get_db), *, team_id: UUID):
    res = TeamRepository.get_by_id(session=session, team_id=team_id)
    return res or None

@team_router.get("/teams", response_model=Optional[TeamBase], status_code=200)
def get_teams(session: Session = Depends(get_db), *, team_id: UUID):
    res = TeamRepository.get(session=session, team_id=team_id)
    return res or None