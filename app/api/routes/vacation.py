from typing import Optional, List
from uuid import UUID
from fastapi import (
    Depends,
    APIRouter,
    HTTPException
)
from sqlalchemy.orm import Session

from app.db.session import get_db
from app.repository.vacation import VacationRepository
from app.schema import VacationBase

vacation_router = APIRouter()

@vacation_router.get("/", response_model=List[Optional[VacationBase]])
def get_vacations(session: Session = Depends(get_db), limit: int=100):
    vacation = VacationRepository.get_all(session=session, limit=limit)
    if not vacation:
        raise HTTPException(status_code=404, detail="Vacation not found")
    return vacation

@vacation_router.get("/{vacation_id}", response_model=Optional[VacationBase])
def get_vacation(session: Session = Depends(get_db), *, vacation_id: UUID):
    vacation = VacationRepository.get_by_id(session=session, vacation_id=vacation_id)
    if not vacation:
        raise HTTPException(status_code=404, detail="Vacation not found")
    return vacation
    
@vacation_router.post("/", response_model=Optional[VacationBase])
def create_vacation(session: Session = Depends(get_db), *, vacation_data: VacationBase):
    vacation = VacationRepository.check_overlap_vacation(session=session, params=vacation_data)
    if vacation[0] == None:
        raise HTTPException(status_code=409, detail="Vacation already exist")
    elif vacation[0]:
        print('routes: ', vacation)
        VacationRepository.create(session=session, params=vacation[1])
    
    return VacationRepository.create(session=session, params=vacation_data)

@vacation_router.delete("/{vacation_id}", response_model=Optional[VacationBase])
def delete_vacation(session: Session = Depends(get_db), *, vacation_id: UUID):
    res = VacationRepository.delete_by_id(session=session, vacation_id=vacation_id)
    return res or None

@vacation_router.put("/{vacation_id}", response_model=Optional[VacationBase])
def update_vacation(session: Session = Depends(get_db), *, vacation_id: UUID, vacation_data: VacationBase):
    vacation = VacationRepository.get_by_id(session=session, vacation_id=vacation_id)
    if not vacation:
        raise HTTPException(status_code=404, detail="Vacation not found")
    return VacationRepository.update_by_id(
        session=session, 
        vacation_id=vacation_id, 
        params=vacation_data
    )

# this route is just for testing purposes
@vacation_router.delete("/all/all", response_model=Optional[VacationBase])
def delete_vacation_all(session: Session = Depends(get_db)):
    res = VacationRepository.delete_all(session=session)
    return res or None
 