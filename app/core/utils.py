from datetime import datetime
from typing import NamedTuple

__all__ = ['is_vacation_overlap_time', 'get_time_range_from_list']


class VacationRange(NamedTuple):
    start_date: datetime
    end_date: datetime

def is_vacation_overlap_time(vacation_1, vacation_2): 
    """Check overlap between two Vacation

    Args:
        vacation_1 (VacationModel): vacation model to check overlap
        vacation_2 (VacationModel): vacation model to check overlap

    Returns:
        bool: overlap found
    """
    v1 = VacationRange(start_date=vacation_1.start_date, end_date=vacation_1.end_date)
    v2 = VacationRange(start_date=vacation_2.start_date, end_date=vacation_2.end_date)
    latest_start = max(v1.start_date, v2.start_date)
    earliest_end = min(v1.end_date, v2.end_date)
    delta = (earliest_end - latest_start).days + 1
    overlap = max(0, delta)
    #print('--overlap: ', vacation_1, vacation_2, overlap)
    if overlap > 1: return True
    else: return False

def get_time_range(vacation_1, vacation_2):
    latest_start = max(datetime(vacation_1.start_date), datetime(vacation_2.start_date))
    earliest_end = min(datetime(vacation_1.end_date), datetime(vacation_2.end_date))
    return latest_start, earliest_end

def get_time_range_from_list(vacations):
    new_vacations = [VacationRange(start_date=x.start_date, end_date=x.end_date) 
                        for x in vacations]
    start_date_values = [x.start_date for x in new_vacations]
    end_date_values = [x.end_date for x in new_vacations]
    latest_start = max(start_date_values)
    earliest_end = min(end_date_values)
    return latest_start, earliest_end
