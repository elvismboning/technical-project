from fastapi import FastAPI, Response, Request, Depends

from app.api import add_app_routes
from app.core.config import settings
from app.model.base import BaseModel

app = FastAPI(
    title=settings.PROJECT_NAME,
    version=settings.VERSION,
)

# Dependency
add_app_routes(app)
