import uuid as uid
from sqlalchemy import Column, DateTime
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import as_declarative
from datetime import datetime

__all__ = ['BaseModel']

class CustomUUID(postgresql.UUID):
    python_type = uid.UUID


@as_declarative()
class BaseModel:
    id = Column(
        CustomUUID(as_uuid=True),
        primary_key=True,
        index=True,
        default=uid.uuid4,
    )
    create_at = Column(DateTime)
    last_update = Column(DateTime, default=datetime.now)


    