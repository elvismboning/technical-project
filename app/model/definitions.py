from sqlalchemy import Column, String, ForeignKey, Table, DateTime, Enum
from enum import Enum as EnumType
from sqlalchemy.orm import relationship
from sqlalchemy_searchable  import SearchQueryMixin, make_searchable
from sqlalchemy_utils.types import TSVectorType

from app.repository.base import BaseRepository
from .base import BaseModel, CustomUUID

__all__ = ["EmployeeModel", "VacationModel", "TeamModel"]

#configure_mappers()
make_searchable(BaseModel.metadata)

class SearchQuery(BaseRepository, SearchQueryMixin):
    pass

# Relational link tables
employee_vacation = Table('employee_vacation_ref', BaseModel.metadata,
    Column('employee_id', ForeignKey('employee.id')),
    Column('vacation_id', ForeignKey('vacation.id'))
)
employee_team = Table('employee_team_ref', BaseModel.metadata,
    Column('employee_id', ForeignKey('employee.id')),
    Column('team_id', ForeignKey('team.id'))
)

# Enum type for Paid leave and vacation type
class PaidLeaveTypeEnum(EnumType):
    rtt = 'rtt'
    normal_paid = 'normal_paid'
    default = None

class VacationTypeEnum(EnumType):
    unpaid_leave = 'unpaid_leave'
    paid_leave = 'paid_leave'


class EmployeeModel(BaseModel):
    __tablename__ = "employee"
    query_class   = SearchQuery

    first_name = Column(String, nullable=False)
    last_name = Column(String)
    email = Column(String, nullable=False, unique=True)
    password  = Column(String, nullable=False)
    vacations = relationship("VacationModel", secondary=employee_vacation,
                             backref="vacation")
    teams = relationship("TeamModel", secondary=employee_team, 
                         backref="team")    
    #search_vector = Column(TSVectorType('first_name', 'last_name', 'email'))

    def __repr__(self) -> str:
        return f"<Employee first_name={self.first_name} last_name={self.last_name} />" 

class TeamModel(BaseModel):
    __tablename__ = "team"
    query_class   = SearchQuery

    name = Column(String, unique=True, nullable=False)
    description = Column(String)
    members = Column(CustomUUID(as_uuid=True), ForeignKey('employee.id'))
    #search_vector = Column(TSVectorType('name', 'description'))

    def __repr__(self) -> str:
        return f"<Team name={self.name} description={self.description} />" 

class VacationModel(BaseModel):
    __tablename__ = "vacation"
    query_class   = SearchQuery

    vacation_type = Column(
        Enum(VacationTypeEnum),
        default=VacationTypeEnum.paid_leave,
        nullable=False
    )
    paid_leave_type = Column(
        Enum(PaidLeaveTypeEnum),
        nullable=False, 
        default=None
    )
    employee_id = Column(
        CustomUUID(as_uuid=True), 
        ForeignKey('employee.id')
    )
    start_date = Column(
        DateTime,
        nullable=False
    )
    end_date = Column(
        DateTime,
        nullable=False
    )

    #search_vector = Column(TSVectorType('vacation_type', 'employee_id', 
    #                        'start_date', 'end_date'))


    def __repr__(self) -> str:
        return f"<Vacation vacation_type={self.vacation_type} employee_id={self.employee_id} />"