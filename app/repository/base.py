class BaseRepository:
    def __init__(self, model):
        self.model = model

    def query(self, session, *a, **kw):
        return session.query(self.model, *a, **kw)
