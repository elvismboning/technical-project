from app.model import EmployeeModel
from app.repository.base import BaseRepository
from datetime import datetime

class _EmployeeRepository(BaseRepository):
    def get_by_id(self, session, employee_id:str):
        """Load one employee from its id

        Args:
            session (Session): Current db session
            employee_id (str): Id param

        Returns:
            EmployeeBase: employee found
        """
        return self.query(session).filter(self.model.id == employee_id).one_or_none()
    
    def get_by_email(self, session, employee_email:str):
        """Load one employee from its email address

        Args:
            session (Session): Current db session
            employee_email (str): Email param

        Returns:
            EmployeeBase: employee found
        """
        return self.query(session).filter(self.model.email == employee_email).one_or_none()
    
    def get_all(self, session, limit:int = 100):
        """Load all employees

        Args:
            session (Session): Current db session
            limit (int, optional): limit to show. Defaults to 100.

        Returns:
            List: List of employee found in the database
        """
        return self.query(session).limit(limit).all()

    def search(self, session, params:dict, limit:int = 100):
        """Search employee

        Args:
            session (Session): Current db session
            params (dict): filter for search 
            limit (int, optional): limit to show. Defaults to 100.

        Returns:
            List: List of employee found in the database
        """
        return self.query(session).filter_by(**params).all()

    def create(self, session, params:dict):
        """Create new employee

        Args:
            session (Session): Current db session
            params (dict): Employee data

        Returns:
            Employee: Employee object
        """
        #hashed_password=fake_hashed_password
        params["create_at"] = datetime.now()
        db_user = EmployeeModel(**dict(params))
        session.add(db_user)
        session.commit()
        session.refresh(db_user)
        return db_user


EmployeeRepository = _EmployeeRepository(model=EmployeeModel)
