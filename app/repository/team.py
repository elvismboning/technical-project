from app.model import TeamModel
from app.repository.base import BaseRepository

# not implemented
class _TeamRepository(BaseRepository):
    def get_by_id(self, session, team_id):
        return self.query(session).filter(self.model.id == team_id)

    def search(self, session, params):
        return self.query(session).filter(self.model.id == team_id)


TeamRepository = _TeamRepository(model=TeamModel)
