from datetime import datetime

from app.model import VacationModel
from app.repository.base import BaseRepository
from app.core.utils import is_vacation_overlap_time, get_time_range_from_list

class _VacationRepository(BaseRepository):
    def get_by_id(self, session, vacation_id):
        return self.query(session).filter(self.model.id == vacation_id).one_or_none()
    
    def get_all(self, session, limit: int=100):
        return self.query(session).limit(limit).all()

    # this is just for testing purposes
    def delete_all(self, session):
        return self.query(session).delete(synchronize_session="fetch")

    def search(self, session, params):
        return self.query(session).filter_by(**params).all()

    def check_overlap_vacation(self, session, params):
        employee_param = {"employee_id": params.employee_id}
        vacations = self.query(session).filter_by(**employee_param).all()
        overlap_vacations = []
        if len(vacations) >= 1:
            for vacation in vacations:
                # check if this vacation exist, skip request if true
                if vacation.start_date == params.start_date \
                    and vacation.end_date == params.end_date:
                    return None, []
                if is_vacation_overlap_time(vacation, params):
                    overlap_vacations.append(vacation)
            overlap_vacations.append(params)
            if len(overlap_vacations) >= 2: 
                new_overlaps = get_time_range_from_list(overlap_vacations)
                #print('<--> ', new_overlaps)
                params['start_date'] = new_overlaps[0]
                params['end_date'] = new_overlaps[1]
                for overlap_vacation in overlap_vacations:
                    self.query(session).filter_by(id=overlap_vacation.id).\
                                        delete(synchronize_session="fetch")
                    #print('-->>> ', overlap_vacation)
                    session.commit()
                return True, params
            else: return False, []
        else: return None, []

    def update_by_id(self, session, vacation_id, params):
        data = dict(params)
        vacation = self.query(session).filter(self.model.id == vacation_id).\
                    update(data, synchronize_session="fetch")
        session.commit()
        return vacation

    def delete_by_id(self, session, vacation_id):
        vacation = self.query(session).filter(self.model.id == vacation_id)
        session.delete(vacation)
        session.commit()
        session.refresh(vacation)
        return vacation

    def create(self, session, params):
        params["create_at"] = datetime.now()
        vacation = VacationModel(**dict(params))
        session.add(vacation)
        session.commit()
        session.refresh(vacation)
        return vacation


VacationRepository = _VacationRepository(model=VacationModel)
