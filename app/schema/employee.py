from pydantic import BaseModel
from pydantic_sqlalchemy import sqlalchemy_to_pydantic

from app.model.definitions import EmployeeModel

__all__ = ['EmployeeBase']

# We ned here to escape password fields in employee model
class EmployeeBase(sqlalchemy_to_pydantic(EmployeeModel)):
    def __init__(self):
        del self.__dict__['password']

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __remove__(self, key):
        del self.__dict__['password']