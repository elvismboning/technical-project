from pydantic import BaseModel
from pydantic_sqlalchemy import sqlalchemy_to_pydantic

from app.model.definitions import TeamModel

__all__ = ['TeamBase']

class TeamBase(sqlalchemy_to_pydantic(TeamModel)):

    def __setitem__(self, key, value):
        self.__dict__[key] = value
