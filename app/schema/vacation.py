from pydantic import BaseModel
from pydantic_sqlalchemy import sqlalchemy_to_pydantic

from app.model.definitions import VacationModel

__all__ = ['VacationBase']

class VacationBase(sqlalchemy_to_pydantic(VacationModel)):

    def __setitem__(self, key, value):
        self.__dict__[key] = value


